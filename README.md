
![movie-app](./public/moviesIcon.png)

Movie-app was web app for code challenge.


## Prerequisites

You need to have installed > NodeJS 8.x and NPM.

## Getting Started


- `npm install`: Install dependencies
- `npm run start`: Run app

## Developed With 

### Front-End
- `ReactJS, Bootstrap` 
### Back-End
- `The movie db API` 

## Developed by 

**Analin Flores** - [Any28Flo](https://github.com/Any28Flo)


## License

This project is licensed under the MIT License 



