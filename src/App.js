import React from 'react';
import './App.css';
import {Container} from 'reactstrap'
import {Router} from "@reach/router"
//Components

import MovieList from "./components/MovieList";
import MovieDetails from "./components/MovieDetails";
import NavComponent from "./components/Nav";
import Footer from "./components/Footer";
const App = () =>{
    return (
        <Container >
            <div className="App">
                <NavComponent/>
                <Router>
                    <MovieList path="/"/>
                    <MovieDetails path="/:id"/>
                </Router>
                <Footer/>
            </div>
        </Container>

);
}
export default App;
