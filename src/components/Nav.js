import React from 'react';
import {Link} from "@reach/router";
import {
    Navbar,
    NavbarBrand
} from 'reactstrap';

const NavComponent = () => {
    const movie_app_icon= require("./../images/moviesIcon.png");

    return (
        <div>
            <Navbar color="light" light expand="md">
                <Link to="/">
                    <NavbarBrand>
                        <img src={`${movie_app_icon}`} className="rounded" alt={`icon-poster`} width="60px"/>
                    </NavbarBrand>
                </Link>
            </Navbar>
        </div>
    );
}
export default NavComponent;