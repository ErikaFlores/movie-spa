import React from "react";

const Footer = () =>{
    return(
        <div>
            <hr/>
            <footer >Made with love <span role="img" aria-label="heart">&#128153;</span> by   <span  role="img" aria-label="girl">&#128105;</span> Analin Flores <span  role="img" aria-label="fire">&#128293;</span></footer>

        </div>

    )
};
export default Footer;