import React from "react";
import {Link} from "@reach/router";
import {
    Col,Card, CardImg, CardBody,
    CardTitle
} from 'reactstrap';

const BASE_URL = "https://image.tmdb.org/t/p/w92/"
const Movie = ({movie}) =>{
    if(!movie) return null
    return(
        <Link to={`/${movie.id}`}>
            <Col >
                <div className="movie">
                    <Card>
                        <CardImg top src={`${BASE_URL}${movie.poster_path}`} />
                        <CardBody>
                            <CardTitle>{movie.title}</CardTitle>
                        </CardBody>
                    </Card>
                </div>
            </Col>
        </Link>


    )
}
export default Movie;