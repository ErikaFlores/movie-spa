import React, {useEffect, useState} from "react";
import {Row} from 'reactstrap'

import Movie from "./Movie";
import PaginationComponent from "./PaginationComponent";
import Spinner from "./Spinner";
//Display the number of movie displayed per page
let numberMoviesPerPage = 19;

const MovieList = () =>{
    const [movieList , setMovieList] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [moviesPerPage] = useState(numberMoviesPerPage);

    const orderByPopularity = (movies) =>{
        const orderMovies =movies.sort((firstMovie, secondMovie) =>{
            return secondMovie.popularity - firstMovie.popularity
        })
        setMovieList(orderMovies);
    }
    useEffect(() =>{
        async function fetchingData() {
            const res = await(fetch(`${ process.env.REACT_APP_URL_API}${currentPage}`))
            res
                .json()
                .then(res => orderByPopularity(res.results))

        }
        fetchingData()
    },[]);
    //Get current movies
    const indexOfLastMovie = currentPage * moviesPerPage;
    const  indexOfFirstMovie = indexOfLastMovie - moviesPerPage;
    const currentMovie = movieList.slice(indexOfFirstMovie,  indexOfLastMovie);
    const paginate = pageNumber => setCurrentPage(pageNumber);
    if( movieList < 1 ) return <Spinner/>
    return(
            <div className="movieList">
                <h2>MovieList</h2>
                <Row xs="1" sm="2" md="3" lg="5">
                    {
                        currentMovie.map( (movie, id) =>{
                            return(  <Movie key={id} movie={movie}/>)
                        })
                    }
                </Row>
                <Row>
                    <PaginationComponent
                        moviesPerPage={moviesPerPage}
                        totalMovies={movieList.length}
                        paginate={paginate}
                    />
                </Row>
            </div>
    )
};
export default MovieList;