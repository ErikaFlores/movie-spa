import React, {useEffect, useState} from "react";
import {Row, Col} from 'reactstrap';
const BASE_URL = "https://image.tmdb.org/t/p/w342";

const MovieDetails = (props) =>{
    const [movieDetails, setMovieDetails ] = useState({});

    useEffect(() =>{
        async function fetchData() {
            const res = await fetch( `https://api.themoviedb.org/3/movie/${props.id}?api_key=${process.env.REACT_APP_API_KEY}&language=en-US`)
            res
                .json()
                .then( res => setMovieDetails(res))
                .catch(e =>console.log(e))
        }

        fetchData()
    }, [])

    const {id, overview, popularity, vote_average, release_date , poster_path} = movieDetails;
    if(!id) return null
    return(

        <div>
            <Row>
                <Col>
                    <h2>{movieDetails.title}</h2>
                </Col>
            </Row>
            <Row className="d-flex justify-content-center">

                    <img src={`${BASE_URL}${poster_path}`} className="rounded" alt={`movie-${id}`}/>

            </Row>
            <Row >

                <Col xs="12" md="8">
                    <h2>Overview</h2>
                    <p className="text-justify">{overview}</p>
                </Col>
                <Col xs="12" md="4">
                    <h3>Rating</h3>
                    <p>{popularity}</p>
                    <h3>Vote Average</h3>
                    <p>{vote_average}</p>
                    <h3>Release date</h3>
                    <p>{release_date}</p>
                </Col>
            </Row>
        </div>
    )
};
export default MovieDetails;